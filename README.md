# EryceFrontendTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

## Design link

[Abstract link](https://share.goabstract.com/bed3c646-b615-4548-8a1c-adeb861473c8)

## Development server

#### To run client and api together

- Open the terminal
- npm install inside planets-api folder
- npm install inside client folder
- Run `./run-app.sh`

#### If you don't have permissions to run the script
- Open the terminal
- Run `chmod +xrw run-app.sh`

## Test requirements

### Style

- Create variables and mixins files
- Try to use scss functions where needed
- Use of Grid and Flexbox for layout
- Use of Angular Material for components
      
### Logic

- Typing is mandatory for basic types (string, number, boolean etc)
- ES6 syntax is preferable
- Creation of dynamic components which will receive and send data through Inputs and Outputs
- Creation of modules
- Create methods for logic
- Don’t repeat yourself (try not to repeat same code more than twice)
- Use camelCase for function names.
- Use camelCase for property names and local variables.

## Task requirements

#### Landing page:

- Create a landing page which will consist of:
  - Grid layout of items
  - Table - here we will list all the items
  - Sorting of items in the table by at least one property
  - Search - used to search the items in the table
  - SWITCH VIEW button which will switch the view between grid and table view of items
  - CREATE button - used to add new item in the table
  - CREATE button will trigger a dialog with a form for creating item

#### Single item page:

- Create a single item page which will show information about a single item

  - Use dynamic routing to go to single item page
  - Under item information show two buttons - EDIT and DELETE
  - EDIT button will trigger a dialog with a form for updating item
  - DELETE button will trigger a confirmation popup

#### Dialog:

- Dialog to show the form for create/edit item
  - Create reactive form for creating/editing item
  - Create CONFIRM and CANCEL buttons
  - CONFIRM button will trigger confirmation popup
  - CANCEL button will close the dialog and disregard all changes made in the form

#### Popup:

- Confirmation popup for CREATE / EDIT / DELETE action
  - Popup must have a message:
    “Are you sure you want to CREATE / EDIT / DELETE [itemName]?
  - Two buttons - CONFIRM and CANCEL
  - CONFIRM button will CREATE / EDIT / DELETE item
  - CANCEL button will close the popup and discard all changes made in the form
