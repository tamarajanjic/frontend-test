import {Component, OnInit} from '@angular/core';
import {PlanetsService} from './services/planets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  planets: any;

  constructor(private planetService: PlanetsService) {
  }

  ngOnInit() {
    this.planetService.getPlanets()
        .subscribe(planets => this.planets = planets);
  }
}
