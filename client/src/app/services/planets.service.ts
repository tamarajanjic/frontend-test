import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PlanetsService {
  constructor(private httpClient: HttpClient) {}

  apiBaseUrl: string = '/api/planets/';
  getPlanets(): Observable<any> {
    return this.httpClient.get<any>(this.apiBaseUrl);
  }

  getPlanet(id: any): Observable<any> {
    return this.httpClient.get<any>(this.getApiUrlWithIdParameter(id));
  }

  createPlanet(payload: any): Observable<any> {
    return this.httpClient.post<any>(this.apiBaseUrl, payload);
  }

  updatePlanet(id: any, payload: any): Observable<any> {
    return this.httpClient.put(this.getApiUrlWithIdParameter(id), payload);
  }

  deletePlanet(id: any): Observable<any> {
    return this.httpClient.delete(this.getApiUrlWithIdParameter(id));
  }

  private getApiUrlWithIdParameter = (id: any) => this.apiBaseUrl + id;
}
